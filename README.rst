##########################
Freesurfer fsaverage files
##########################

This repository contains an unpacked version of the download of the
``fsaverage`` archive from
https://surfer.nmr.mgh.harvard.edu/ftp/data/fsaverage.tar.gz.

Please see the LICENSE.rst file.
